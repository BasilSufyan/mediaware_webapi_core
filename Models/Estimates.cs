﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Estimates
    {
        public string Site { get; set; }
        public string RoToCompany { get; set; }
        public string Adtype { get; set; }
        public string Area { get; set; }
        public string ScheduledFrom { get; set; }
        public string ScheduledTo { get; set; }
        public decimal Rate { get; set; }
        public decimal Unit { get; set; }
        public string Company { get; set; }
        public decimal Duration { get; set; }
        public string addDesc { get; set; }
        public string addLan { get; set; }
        public string RentUnit { get; set; }
        public string caption { get; set; }

    }
}
