﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
//
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAllOrigins")]
    public class EstimatesController : ControllerBase
    {
        private readonly IConfiguration configuration;

        //test static string if DB not working.
        static List<Estimates> estimates = new List<Estimates>()
        {
            new Estimates() { Site = "JAGSHWARA - KURNOOL", RoToCompany = "MOTS MEDIA & ENTERTAINMENT PVT. LTD.",
                ScheduledFrom = "17/12/2015", ScheduledTo = "2019-01-01",Rate = 5000,
                Unit = 15, Company = "MOTIF  &  NTERTAINMENT PVT. LTD.",
                Duration = 50,addDesc = "Something",addLan = "Chin",RentUnit = "Days",caption = "8070" },

        };

        public EstimatesController(IConfiguration config)
        {
            this.configuration = config;
        }

        [HttpGet]
        public IEnumerable<Estimates> Get()
        {
            List<Estimates> estList = new List<Estimates>();
            try
            {
                string config = configuration.GetConnectionString("DefaultConnectionString");



                SqlConnection connection = new SqlConnection(config);
                connection.Open();

                SqlCommand cmd = new SqlCommand("Select * from EstimatesTBL", connection);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Estimates est = new Estimates();
                    est.Site = reader["Site"].ToString();
                    est.Unit = Convert.ToDecimal(reader["Unit"].ToString());
                    est.Rate = Convert.ToDecimal(reader["Rate"].ToString());
                    est.RentUnit = reader["RentUnit"].ToString();
                    est.RoToCompany = reader["RoToCompany"].ToString();
                    est.ScheduledFrom = reader["ScheduledFrom"].ToString();
                    est.ScheduledTo = reader["ScheduledTo"].ToString();
                    est.Duration = Convert.ToDecimal(reader["Duration"].ToString());
                    est.Company = reader["Company"].ToString();
                    est.Area = reader["Area"].ToString();
                    est.addDesc = reader["addDesc"].ToString();
                    est.addLan = reader["addLan"].ToString();
                    est.Adtype = reader["Adtype"].ToString();
                    est.caption = reader["caption"].ToString();

                    estList.Add(est);
                }


                connection.Close();
            }
            catch (Exception ex)
            {
                return null;
            }

            return estList;
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody] Estimates[] newEntry)
        {
            try
            {
                string config = configuration.GetConnectionString("DefaultConnectionString");
                SqlConnection connection = new SqlConnection(config);
                connection.Open();
                foreach (Estimates est in newEntry)
                {
                    string query = "insert into EstimatesTBL (Site,RoToCompany,Adtype,Area,ScheduledFrom,ScheduledTo,Rate,Unit,Company,Duration,addDesc,addLan,RentUnit,caption)"
                    + "values('" + est.Site + "','" + est.RoToCompany + "','" + est.Adtype + "','" + est.Area + "','" + est.ScheduledFrom + "','" + est.ScheduledTo + "'," + est.Rate + "," + est.Unit + ",'" + est.Company + "'," + est.Duration + ",'" + est.addDesc + "','" + est.addLan + "','" + est.RentUnit + "','" + est.caption + "')";

                    //string query = "insert into EstimatesTBL (Site,RoToCompany) values('Site','ROTOCompany')";

                    SqlCommand myCommand = new SqlCommand(query, connection);
                    myCommand.ExecuteNonQuery();

                    //estimates.Add(est);
                }


                connection.Close();

                //test without DB
                //foreach (Estimates est in newEntry)
                //{
                //    estimates.Add(est);
                //}

                return new HttpResponseMessage()
                {
                    Content = new StringContent("POST: Test message")
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage()
                {
                    Content = new StringContent("Error Message")
                };
            }


        }

        [HttpDelete]
        public void Delete()
        {
            try
            {
                string config = configuration.GetConnectionString("DefaultConnectionString");
                SqlConnection connection = new SqlConnection(config);
                connection.Open();
                SqlCommand cmd = new SqlCommand("truncate table EstimatesTBL", connection);
                cmd.ExecuteReader();

                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}